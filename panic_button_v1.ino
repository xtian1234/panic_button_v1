#include <FlashStorage.h>
#include <Adafruit_DotStar.h>
#include <SPI.h>
#include <ArduinoUniqueID.h>
#include "HID-Project.h"
#include "OneButton.h"

FlashStorage(singlePressVar, int);
FlashStorage(doublePressVar, int);
FlashStorage(longPressVar, int);
FlashStorage(ledBrightnessVar, float);
FlashStorage(singlePressKeyReg, KeyboardKeycode);
FlashStorage(doublePressKeyReg, KeyboardKeycode);
FlashStorage(longPressKeyReg, KeyboardKeycode);
FlashStorage(singlePressCtrlPressedReg, bool);
FlashStorage(singlePressShiftPressedReg, bool);
FlashStorage(singlePressAltPressedReg, bool);
FlashStorage(doublePressCtrlPressedReg, bool);
FlashStorage(doublePressShiftPressedReg, bool);
FlashStorage(doublePressAltPressedReg, bool);
FlashStorage(longPressCtrlPressedReg, bool);
FlashStorage(longPressShiftPressedReg, bool);
FlashStorage(longPressAltPressedReg, bool);
FlashStorage(hidEnabledReg, int);
FlashStorage(comEnabledReg, int);
FlashStorage(sPressTimeReg, int);
FlashStorage(dPressTimeReg, int);
FlashStorage(lPressTimeReg, int);

String sw = "1.0.1";
String hw = "1.0";
String cmd        = "";
String reg        = "";
String value      = "";
String tempString = "";
String tempKeyString = "";
String SerialCMD[5];
int SerialArguments = 0;
String regArray[26] = {"sPress", "dPress", "lPress", "ledBrightness", "spKey",
                       "dpKey", "lpKey", "com", "hid", "sShiftKey",
                       "dShiftKey", "lShiftKey", "sCtlKey", "dCtlKey", "lCtlKey",
                       "sAltKey", "dAltKey", "lAltKey", "version", "ver",
                       "ledColor", "setLED", "runningColor", "fadeColor", "fixColor", "uniqueID"
                      };

bool singlePressCtrlPressed = false;
bool singlePressShiftPressed = false;
bool singlePressAltPressed = false;
bool doublePressCtrlPressed = false;
bool doublePressShiftPressed = false;
bool doublePressAltPressed = false;
bool longPressCtrlPressed = false;
bool longPressShiftPressed = false;
bool longPressAltPressed = false;

bool hidEnabled = 1;
bool comEnabled = 1;

// Note: the area of flash memory reserved for the variable is
// lost every time the sketch is uploaded on the board.
int singlePress           = 9;
int doublePress           = 11;
int longPress             = 22;
KeyboardKeycode singlePressKey    = KEY_1;
KeyboardKeycode doublePressKey    = KEY_2;
KeyboardKeycode longPressKey      = KEY_3;
int sPressTime = 1;
int dPressTime = 1;
int lPressTime = 1;
int tempTimer = 0;
int LEDTimer = 0;
int ledColorTimer = 0;
unsigned long LEDTimerStart = 0;
bool LEDTimerEN = false;
unsigned long hexValue = 0x00000000;
char colorValueArray[8];
int ledValue = 1;

const int panicButton     = 2;  //Digital pin 2

//DOTSTAR LEDs
#define CALL_BUTTON_NUMPIXELS 12 // Number of LEDs
#define MAX_NUMPIXELS         13 // MAX Number of LEDs

float MAX_BRIGHTNESS = 100;
uint8_t callButton_start = 0;
uint8_t callButton_end = 12;
int head = 0;
int tail = -7;
bool runningLights_Stat = false;
bool fadeInOut_Stat = false;
bool lights_EN = false;
int ledBrightness = 0;
bool fadeIN = false;
bool fadeOUT = false;

uint32_t GREEN        = 0xFF0000;
uint32_t RED          = 0x00FF00;
uint32_t BLUE         = 0x0000FF;
uint32_t YELLOW       = 0xFFFF00;
uint32_t ORANGE       = 0xA5FF00;
uint32_t CYAN         = 0xFF00FF;
uint32_t WHITE        = 0xFFFFFF;
uint32_t runningColor = 0x000000;

Adafruit_DotStar strip(MAX_NUMPIXELS, DOTSTAR_BRG);
OneButton button(panicButton, true, false);

void startUp_sequence();
void load_lastConfig();
void load_Config();
void singleclick();
void doubleclick();
void longclick();
void startUP_LightSequence();
void clearCallButtonLED();
void SetCallButtonLED(uint32_t color);
void runningLights(uint32_t color);
void FadeInOut(uint32_t color);
void commandHandler();
void ledLightHandler(int lightSequence);
KeyboardKeycode KeyEquivalentHandler(String cmdValue);
String keyboardKeycodetoString(KeyboardKeycode key);

void setLEDTimer(int timerValue);
void setFadeColor(String colorValue);
void setRunningLED(String colorValue);
void setFixLED(String colorValue);
void setLEDColor(int ledNum, String colorValue);
bool is_oneZero_ParamValid(String param);
bool checkLEDParameter(int param);
bool checkRegister(String regName);

void getspKey();
void getver();

void setup() {
  Serial.begin(115200);
  delay(100);
  UniqueIDdump(Serial);
  Serial.print("UniqueID: ");
  Keyboard.begin();
  strip.begin(); // Initialize pins for output
  clearCallButtonLED();

  button.attachDoubleClick(doubleclick);            // link the function to be called on a doubleclick event.
  button.attachClick(singleclick);                  // link the function to be called on a singleclick event.
  button.attachLongPressStop(longclick);            // link the function to be called on a longpress event.

  startUp_sequence();
  load_lastConfig();

}

void loop() {

  if (Serial.available() > 0) {
    commandHandler();
  }

  /***********************************************************/
  if (runningLights_Stat) {
    runningLights(runningColor);
  }
  if (fadeInOut_Stat) {
    FadeInOut(runningColor);
  }

  if (LEDTimerEN) {
    if ((millis() - LEDTimerStart) >= LEDTimer) {
      ledLightHandler(1);
      LEDTimerEN = false;
      LEDTimer = 0;
    }
  }
  /***********************************************************/

  /************************PANIC BUTTON************************/
  button.tick();
  /***********************************************************/

  delay(35);

}
/************************END OF LOOP************************/

void startUp_sequence() {

  startUP_LightSequence();

}

void load_lastConfig() {

  load_Config();
  Serial.println("Loaded last configuration");
}

void load_Config() {

  if (ledBrightnessVar.read() != NULL) {
    MAX_BRIGHTNESS = ledBrightnessVar.read();
  }
  if (singlePressVar.read() != NULL) {
    singlePress = singlePressVar.read();
  }
  if (doublePressVar.read() != NULL) {
    doublePress = doublePressVar.read();
  }
  if (longPressVar.read() != NULL) {
    longPress   = longPressVar.read();
  }
  if (singlePressKeyReg.read() != NULL) {
    singlePressKey = singlePressKeyReg.read();
  }
  if (doublePressKeyReg.read() != NULL) {
    doublePressKey = doublePressKeyReg.read();
  }
  if (longPressKeyReg.read() != NULL) {
    longPressKey = longPressKeyReg.read();
  }
  if (singlePressCtrlPressedReg.read() != NULL) {
    singlePressCtrlPressed = singlePressCtrlPressedReg.read();
  }
  if (singlePressShiftPressedReg.read() != NULL) {
    singlePressShiftPressed = singlePressShiftPressedReg.read();
  }
  if (singlePressAltPressedReg.read() != NULL) {
    singlePressAltPressed = singlePressAltPressedReg.read();
  }
  if (doublePressCtrlPressedReg.read() != NULL) {
    doublePressCtrlPressed = doublePressCtrlPressedReg.read();
  }
  if (doublePressShiftPressedReg.read() != NULL) {
    doublePressShiftPressed = doublePressShiftPressedReg.read();
  }
  if (doublePressAltPressedReg.read() != NULL) {
    doublePressAltPressed = doublePressAltPressedReg.read();
  }
  if (longPressCtrlPressedReg.read() != NULL) {
    longPressCtrlPressed = longPressCtrlPressedReg.read();
  }
  if (longPressShiftPressedReg.read() != NULL) {
    longPressShiftPressed = longPressShiftPressedReg.read();
  }
  if (longPressAltPressedReg.read() != NULL) {
    longPressAltPressed = longPressAltPressedReg.read();
  }
  if (sPressTimeReg.read() > 0) {
    sPressTime = sPressTimeReg.read();
  }
  if (dPressTimeReg.read() > 0) {
    dPressTime = dPressTimeReg.read();
  }
  if (lPressTimeReg.read() > 0) {
    lPressTime = lPressTimeReg.read();
  }
  if (hidEnabledReg.read() != NULL) {
    hidEnabled = hidEnabledReg.read() - 1;
  }
  if (comEnabledReg.read() != NULL) {
    comEnabled = comEnabledReg.read() - 1;
  }
}

/************************************PANIC BUTTON************************************/

void singleclick() {                                //when the button is clicked
  if (comEnabled == 1) Serial.println("E:CLICK_SINGLE");
  if (hidEnabled == 1) {

    String tempkey = "";
    if (singlePressCtrlPressed) {
      Keyboard.press(KEY_LEFT_CTRL);
      tempkey += "CTRL+";
    }
    if (singlePressShiftPressed) {
      Keyboard.press(KEY_LEFT_SHIFT);
      tempkey += "SHIFT+";
    }
    if (singlePressAltPressed) {
      Keyboard.press(KEY_LEFT_ALT);
      tempkey += "ALT+";
    }
    tempkey += keyboardKeycodetoString(singlePressKey);         /******************************/
    Keyboard.press(singlePressKey);
    if (!singlePressCtrlPressed && !singlePressAltPressed) {
      Keyboard.press(KEY_ENTER);
    }
    Serial.println("L:Single Key Sent ->" + tempkey );          /******************************/
    delay(100);
    Keyboard.releaseAll();
    tempkey = "";
  }
  ledLightHandler(singlePress);
  if (sPressTime > 1) {
    setLEDTimer(sPressTime);
  }
}

void doubleclick() {                                //when button is double-clicked
  if (comEnabled == 1) {
    Serial.println("E:CLICK_DOUBLE");
  }
  if (hidEnabled == 1) {
    String tempkey = "";
    if (doublePressCtrlPressed) {
      Keyboard.press(KEY_LEFT_CTRL);
      tempkey += "CTRL+";
    }
    if (doublePressShiftPressed) {
      Keyboard.press(KEY_LEFT_SHIFT);
      tempkey += "SHIFT+";
    }
    if (doublePressAltPressed) {
      Keyboard.press(KEY_LEFT_ALT);
      tempkey += "ALT+";
    }
    tempkey += keyboardKeycodetoString(doublePressKey);       /******************************/
    Keyboard.press(doublePressKey);
    if (!doublePressCtrlPressed && !doublePressAltPressed) {
      Keyboard.press(KEY_ENTER);
    }
    //Serial.println("L:Double Key Sent ->" + doublePressKey);
    delay(100);
    Keyboard.releaseAll();
    Serial.println("L:Double Key Sent ->" + tempkey );
    tempkey = "";
  }
  ledLightHandler(doublePress);
  if (dPressTime > 1) {
    setLEDTimer(dPressTime);
  }
}

void longclick() {                                  //when buton is long-pressed

  if (comEnabled == 1)
  {
    Serial.println("E:CLICK_LONG");
  }
  if (hidEnabled == 1) {
    String tempkey = "";
    if (longPressCtrlPressed) {
      Keyboard.press(KEY_LEFT_CTRL);
      tempkey += "CTRL+";
    }
    if (longPressShiftPressed) {
      Keyboard.press(KEY_LEFT_SHIFT);
      tempkey += "SHIFT+";
    }
    if (longPressAltPressed) {
      Keyboard.press(KEY_LEFT_ALT);
      tempkey += "ALT+";
    }
    tempkey += keyboardKeycodetoString(longPressKey);     /******************************/
    Keyboard.press(longPressKey);
    if (!longPressCtrlPressed && !longPressAltPressed) {
      Keyboard.press(KEY_ENTER);
    }

    delay(100);
    Keyboard.releaseAll();
    Serial.println("L:Long Key Sent ->" + tempkey );
  }

  ledLightHandler(longPress);
  if (lPressTime > 1) {
    setLEDTimer(lPressTime);
  }
}

/************************************LEDs************************************/

void startUP_LightSequence() {
  SetCallButtonLED(RED);
  delay(500);
  SetCallButtonLED(BLUE);
  delay(500);
  SetCallButtonLED(GREEN);
  delay(500);
  clearCallButtonLED();
}

void clearCallButtonLED() {
  for (int i = 0; i < MAX_NUMPIXELS; i++) {
    strip.setPixelColor(i, 0x000000);
    strip.show();
  }
}

void SetCallButtonLED(uint32_t color) {

  strip.setBrightness(MAX_BRIGHTNESS);
  for (int i = 0; i < CALL_BUTTON_NUMPIXELS; i++) {
    strip.setPixelColor(i, color);
  }
  strip.show();
}

void runningLights(uint32_t color) {

  uint8_t r, g, b;
  b = color;
  b = (MAX_BRIGHTNESS / 255.0) * b;
  color = color >> 8;
  r = color;
  r = (MAX_BRIGHTNESS / 255.0) * r;
  color = color >> 8;
  g = color;
  g = (MAX_BRIGHTNESS / 255.0) * g;

  strip.setPixelColor(head, g, r, b);
  strip.setPixelColor(tail, 0x000000);
  strip.show();                     // Refresh strip
  delay(20);                        // Pause 20 milliseconds (~50 FPS)

  if (++head >= callButton_end) {        // Increment head index.  Off end of strip?
    head = callButton_start;                       //  Yes, reset head index to start
  }
  if (++tail >= callButton_end) {
    tail = callButton_start;    // Increment, reset tail index
  }
}

void FadeInOut(uint32_t color) {

  for (int i = 0; i < CALL_BUTTON_NUMPIXELS; i++) {
    strip.setPixelColor(i, color);
  }
  strip.setBrightness(ledBrightness);
  strip.show();
  if (fadeIN) {
    ledBrightness += 4;
    if (ledBrightness >= MAX_BRIGHTNESS) {
      fadeIN = false;
      fadeOUT = true;
    }
  }
  if (fadeOUT) {
    ledBrightness -= 3;
    if (ledBrightness <= 0) {
      ledBrightness = 0;
      fadeOUT = false;
      fadeIN = true;
    }
  }
}

/************************************ New Serial Command Handler ************************************/
void commandHandler() {
  SerialArguments = 0;
  char inChar = Serial.read();
  if (inChar == '!') {
    while (Serial.available()) {
      SerialCMD[SerialArguments]   = Serial.readStringUntil(' ');
      SerialArguments++;
    }
    cmd = SerialCMD[0];
    cmd.trim();
    if (SerialArguments <= 1) {
      Serial.println("C:Not enough Arguments.");
    } else {                                         // <-----START of else statement for SerialArguments>1
      if (cmd == "get" || cmd == "set") {             // <-----START of cmd=get/set statement
        if (SerialArguments == 2) {                   // <-----START of 2 arg
          if (cmd == "get") {
            reg   = SerialCMD[1];
            reg.trim();
            if (checkRegister(reg)) {
              if (reg == "sPress") {
                Serial.print(singlePress);
                Serial.print(" ");
                Serial.println(sPressTime - 1);
              }
              if (reg == "dPress") {
                Serial.print(doublePress);
                Serial.print(" ");
                Serial.println(dPressTime - 1);
              }
              if (reg == "lPress") {
                Serial.print(longPress);
                Serial.print(" ");
                Serial.println(lPressTime - 1);
              }
              if (reg == "ledBrightness") {
                Serial.println(MAX_BRIGHTNESS);
              }

              if (reg == "version" || reg == "ver") getver();
              if (reg == "spKey") getspKey();

              /*if (reg == "spKey\n") {
                tempString = keyboardKeycodetoString(singlePressKey);
                if (singlePressCtrlPressed) {
                  tempKeyString = tempKeyString + "ctrl+";
                }
                if (singlePressShiftPressed) {
                  tempKeyString = tempKeyString + "shift+";
                }
                if (singlePressAltPressed) {
                  tempKeyString = tempKeyString + "alt+";
                }
                Serial.println(tempKeyString + tempString);
                tempKeyString = "";
                }*/
              if (reg == "dpKey") {
                tempString = keyboardKeycodetoString(doublePressKey);
                if (doublePressCtrlPressed) {
                  tempKeyString = tempKeyString + "ctrl+";
                }
                if (doublePressShiftPressed) {
                  tempKeyString = tempKeyString + "shift+";
                }
                if (doublePressAltPressed) {
                  tempKeyString = tempKeyString + "alt+";
                }
                Serial.println(tempKeyString + tempString);
                tempKeyString = "";
              }
              if (reg == "lpKey") {
                tempString = keyboardKeycodetoString(longPressKey);
                if (longPressCtrlPressed) {
                  tempKeyString = tempKeyString + "ctrl+";
                }
                if (longPressShiftPressed) {
                  tempKeyString = tempKeyString + "shift+";
                }
                if (longPressAltPressed) {
                  tempKeyString = tempKeyString + "alt+";
                }
                Serial.println(tempKeyString + tempString);
                tempKeyString = "";
              }
              if (reg == "com") {
                Serial.println(comEnabled);
                //   Serial.println("\n");
              }
              if (reg == "hid") {
                Serial.println(hidEnabled);
                //  Serial.println("\n");
              }
              if (reg == "uniqueID") {
                getUniqueID();
              }
            }
            else {
              Serial.println("C:Not a valid register!");
            }
          }  //                       <----END OF GET
        } //                           <----END OF Set 2 argu condition
        if (SerialArguments >= 3) {
          if (cmd == "set") {
            reg   = SerialCMD[1];
            value = SerialCMD[2];
            value.trim();
            if (checkRegister(reg)) {
              Serial.print("L:Set->" +  reg + ":" + value + ":");
              if (reg == "sPress") {              //Single Press LED Value
                int sValue = value.toInt();
                if (checkLEDParameter(sValue)) {
                  singlePressVar.write(sValue);
                  sPressTimeReg.write(1);
                  Serial.print("Status:Updated Single Press LED equivalent value to: ");
                  Serial.println(sValue);
                } else {
                  Serial.println("C:Invalid LED Lights parameter!");
                }
              }
              if (reg == "dPress") {              //Double Press LED Value
                int dValue = value.toInt();
                if (checkLEDParameter(dValue)) {
                  doublePressVar.write(dValue);
                  dPressTimeReg.write(1);
                  Serial.print("Updated Double Press LED equivalent value to: ");
                  Serial.println(dValue);
                } else {
                  Serial.println("C:Invalid LED Lights parameter!");
                }
              }
              if (reg == "lPress") {              //Long Press LED Value
                int lValue = value.toInt();
                if (checkLEDParameter(lValue)) {
                  longPressVar.write(lValue);
                  lPressTimeReg.write(1);
                  Serial.print("Updated Long Press LED equivalent value to: ");
                  Serial.println(lValue);
                } else {
                  Serial.println("C:Invalid LED Lights parameter!");
                }
              }
              if (reg == "ledColor") {              //set LED color via serial
                ledValue = value.toInt();
                if (checkLEDParameter(ledValue)) {
                  ledLightHandler(ledValue);
                  Serial.print("Updated LED equivalent value to: ");
                  Serial.println(value);
                } else {
                  Serial.println("C:Invalid LED Lights parameter!");
                }
              }
              if (reg == "runningColor") {              //set running LED color via serial
                setRunningLED(value);
                Serial.print("Updated Running LED equivalent value to: ");
                Serial.println(value);
              }
              if (reg == "fadeColor") {              //set fade in/out LED color via serial
                setFadeColor(value);
                Serial.print("Updated Fade In/Out LED equivalent value to: ");
                Serial.println(value);
              }
              if (reg == "fixColor") {              //set fix/steady LED color via serial
                setFixLED(value);
                Serial.print("Updated All LED equivalent value to: ");
                Serial.println(value);
              }
              if (reg == "ledBrightness") {       //LED Brightness
                int bValue = value.toInt();
                ledBrightnessVar.write(bValue);
                Serial.print("Updated LED Brightness Value to: ");
                Serial.println(bValue);
              }
              if (reg == "spKey") {
                if (value == "ctrl") {
                  singlePressCtrlPressed = !singlePressCtrlPressed;
                  singlePressCtrlPressedReg.write(singlePressCtrlPressed);
                  if (singlePressCtrlPressed) {
                    Serial.println("Ctrl Key pressed!");
                  } else {
                    Serial.println("Ctrl Key released!");
                  }
                } else if (value == "shift") {
                  singlePressShiftPressed = !singlePressShiftPressed;
                  singlePressShiftPressedReg.write(singlePressShiftPressed);
                  if (singlePressShiftPressed) {
                    Serial.println("Shift Key pressed!");
                  } else {
                    Serial.println("Shift Key released!");
                  }
                } else if (value == "alt") {
                  singlePressAltPressed = !singlePressAltPressed;
                  singlePressAltPressedReg.write(singlePressAltPressed);
                  if (singlePressAltPressed) {
                    Serial.println("Alt Key pressed!");
                  } else {
                    Serial.println("Alt Key released!");
                  }
                } else {
                  singlePressKey = KeyEquivalentHandler(value);
                  singlePressKeyReg.write(singlePressKey);

                  if (singlePressCtrlPressed) {
                    tempKeyString = tempKeyString + "ctrl+";
                  }
                  if (singlePressShiftPressed) {
                    tempKeyString = tempKeyString + "shift+";
                  }
                  if (singlePressAltPressed) {
                    tempKeyString = tempKeyString + "alt+";
                  }
                  Serial.println(tempKeyString + value);
                  Serial.print("Single Press equivalent keystroke changed to: " + tempKeyString + value);
                  tempKeyString = "";
                }
              }
              if (reg == "dpKey") {
                if (value == "ctrl") {
                  doublePressCtrlPressed = !doublePressCtrlPressed;
                  doublePressCtrlPressedReg.write(doublePressCtrlPressed);
                  if (doublePressCtrlPressed) {
                    Serial.println("Ctrl Key pressed!");
                  } else {
                    Serial.println("Ctrl Key released!");
                  }
                } else if (value == "shift") {
                  doublePressShiftPressed = !doublePressShiftPressed;
                  doublePressShiftPressedReg.write(doublePressShiftPressed);
                  if (doublePressShiftPressed) {
                    Serial.println("Shift Key pressed!");
                  } else {
                    Serial.println("Shift Key released!");
                  }
                } else if (value == "alt") {
                  doublePressAltPressed = !doublePressAltPressed;
                  doublePressAltPressedReg.write(doublePressAltPressed);
                  if (doublePressAltPressed) {
                    Serial.println("Alt Key pressed!");
                  } else {
                    Serial.println("Alt Key released!");
                  }
                } else {
                  doublePressKey = KeyEquivalentHandler(value);
                  doublePressKeyReg.write(doublePressKey);
                  Serial.print("Double Press equivalent keystroke changed to: ");
                  if (doublePressCtrlPressed) {
                    tempKeyString = tempKeyString + "ctrl+";
                  }
                  if (doublePressShiftPressed) {
                    tempKeyString = tempKeyString + "shift+";
                  }
                  if (doublePressAltPressed) {
                    tempKeyString = tempKeyString + "alt+";
                  }
                  Serial.println(tempKeyString + value);
                  tempKeyString = "";
                }
              }
              if (reg == "lpKey") {
                if (value == "ctrl") {
                  longPressCtrlPressed = !longPressCtrlPressed;
                  longPressCtrlPressedReg.write(longPressCtrlPressed);
                  if (longPressCtrlPressed) {
                    Serial.println("L:Ctrl Key pressed!");
                  } else {
                    Serial.println("L:Ctrl Key released!");
                  }
                } else if (value == "shift") {
                  longPressShiftPressed = !longPressShiftPressed;
                  longPressShiftPressedReg.write(longPressShiftPressed);
                  if (longPressShiftPressed) {
                    Serial.println("L:Shift Key pressed!");
                  } else {
                    Serial.println("L:Shift Key released!");
                  }
                } else if (value == "alt") {
                  longPressAltPressed = !longPressAltPressed;
                  longPressAltPressedReg.write(longPressAltPressed);
                  if (longPressAltPressed) {
                    Serial.println("L:Alt Key pressed!");
                  } else {
                    Serial.println("L:Alt Key released!");
                  }
                } else {
                  longPressKey = KeyEquivalentHandler(value);
                  longPressKeyReg.write(longPressKey);
                  Serial.print("L:Long Press equivalent keystroke changed to: ");
                  if (longPressCtrlPressed) {
                    tempKeyString = tempKeyString + "ctrl+";
                  }
                  if (longPressShiftPressed) {
                    tempKeyString = tempKeyString + "shift+";
                  }
                  if (longPressAltPressed) {
                    tempKeyString = tempKeyString + "alt+";
                  }
                  Serial.println(tempKeyString + value);
                  tempKeyString = "";
                }
              }

              if ( reg == "hid") {
                if (is_oneZero_ParamValid(value)) {
                  if (value == "0") {
                    hidEnabled = 0;
                    Serial.println("HID Disabled");       /*************************/
                  } else {
                    hidEnabled = 1;
                    Serial.println("HID Enabled");       /*************************/
                  }
                  hidEnabledReg.write(hidEnabled + 1);
                } else {
                  Serial.println("C:Invalid Parameter!");
                }
              }
              if ( reg == "com") {
                if (is_oneZero_ParamValid(value)) {
                  if (value == "0") {
                    comEnabled = 0;
                    Serial.print("COM Disabled");
                  } else {
                    comEnabled = 1;
                    Serial.print("COM Enabled");
                  }
                  comEnabledReg.write(comEnabled + 1);
                } else {
                  Serial.println("C:Invalid Parameter!");
                }
              }
              //modifier key handler
              if ( reg == "sShiftKey") {
                if (is_oneZero_ParamValid(value)) {
                  if (value == "0") {
                    singlePressShiftPressed = false;
                    Serial.print("Status: Shift key for single click released!");
                  } else {
                    singlePressShiftPressed = true;
                    Serial.print("Status: Shift key for single click pressed!");
                  }
                  singlePressShiftPressedReg.write(singlePressShiftPressed);
                } else {
                  Serial.println("C:Invalid Parameter!");
                }
              }
              //modifier key handler
              if ( reg == "dShiftKey") {
                if (is_oneZero_ParamValid(value)) {
                  if (value == "0") {
                    doublePressShiftPressed = false;
                    Serial.print("Status: Shift key for double click released!");
                  } else {
                    doublePressShiftPressed = true;
                    Serial.print("Status: Shift key for double click pressed!");
                  }
                  doublePressShiftPressedReg.write(doublePressShiftPressed);
                } else {
                  Serial.println("C:Invalid Parameter!");
                }
              }
              //modifier key handler
              if ( reg == "lShiftKey") {
                if (is_oneZero_ParamValid(value)) {
                  if (value == "0") {
                    longPressShiftPressed = false;
                    Serial.print("Status: Shift key for long click released!");
                  } else {
                    longPressShiftPressed = true;
                    Serial.print("Status: Shift key for long click pressed!");
                  }
                  longPressShiftPressedReg.write(longPressShiftPressed);
                } else {
                  Serial.println("C:Invalid Parameter!");
                }
              }
              if ( reg == "sCtlKey") {
                if (is_oneZero_ParamValid(value)) {
                  if (value == "0") {
                    singlePressCtrlPressed = false;
                    Serial.print("Status: Ctl key for single click released!");
                  } else {
                    singlePressCtrlPressed = true;
                    Serial.print("Status: Ctl  key for single click pressed!");
                  }
                  singlePressCtrlPressedReg.write(singlePressCtrlPressed);
                } else {
                  Serial.println("C:Invalid Parameter!");
                }
              }
              //modifier key handler
              if ( reg == "dCtlKey") {
                if (is_oneZero_ParamValid(value)) {
                  if (value == "0") {
                    doublePressCtrlPressed = false;
                    Serial.print("Status: Ctl key for double click released!");
                  } else {
                    doublePressCtrlPressed = true;
                    Serial.print("Status: Ctl key for double click pressed!");
                  }
                  doublePressCtrlPressedReg.write(doublePressCtrlPressed);
                } else {
                  Serial.println("C:Invalid Parameter!");
                }
              }
              //modifier key handler
              if ( reg == "lCtlKey") {
                if (is_oneZero_ParamValid(value)) {
                  if (value == "0") {
                    longPressCtrlPressed = false;
                    Serial.print("Status: Ctl key for long click released!");
                  } else {
                    longPressCtrlPressed = true;
                    Serial.print("Status: Ctl key for long click pressed!");
                  }
                  longPressCtrlPressedReg.write(longPressCtrlPressed);
                } else {
                  Serial.println("C:Invalid Parameter!");
                }
              }
              if ( reg == "sAltKey") {
                if (is_oneZero_ParamValid(value)) {
                  if (value == "0") {
                    singlePressAltPressed = false;
                    Serial.print("Status: Alt key for single click released!");
                  } else {
                    singlePressAltPressed = true;
                    Serial.print("Status: Alt key for single click pressed!");
                  }
                  singlePressAltPressedReg.write(singlePressAltPressed);
                } else {
                  Serial.println("C:Invalid Parameter!");
                }
              }
              //modifier key handler
              if ( reg == "dAltKey") {
                if (is_oneZero_ParamValid(value)) {
                  if (value == "0") {
                    doublePressAltPressed = false;
                    Serial.print("Status: Alt key for double click released!");
                  } else {
                    doublePressAltPressed = true;
                    //comEnabled = true;
                    Serial.print("Status: Alt key for double click pressed!");
                  }
                  doublePressAltPressedReg.write(doublePressAltPressed);
                } else {
                  Serial.println("C:Invalid Parameter!");
                }
              }
              //modifier key handler
              if ( reg == "lAltKey") {
                if (is_oneZero_ParamValid(value)) {
                  if (value == "0") {
                    longPressAltPressed = false;
                    Serial.print("Status: Alt key for long click released!");
                  } else {
                    longPressAltPressed = false;
                    Serial.print("Status: Alt key for long click pressed!");
                  }
                  longPressAltPressedReg.write(doublePressAltPressed);
                } else {
                  Serial.println("C:Invalid Parameter!");
                }
              }

              if (SerialArguments >= 4) {
                tempTimer = SerialCMD[3].toInt();
                if (reg == "sPress") {              //Single Press LED Value
                  sPressTimeReg.write(tempTimer + 1);
                }
                if (reg == "dPress") {              //Double Press LED Value
                  dPressTimeReg.write(tempTimer + 1);
                }
                if (reg == "lPress") {              //Long Press LED Value
                  lPressTimeReg.write(tempTimer + 1);
                }
                if (reg == "ledColor" || reg == "runningColor" || reg == "fadeColor" || reg == "fixColor") {              //set duration when LED is controlled via serial
                  if (tempTimer > 0) {
                    setLEDTimer(tempTimer + 1);
                  }
                }
                if (reg == "setLED") {
                  int mLedNum = value.toInt();
                  int tempTimer2 = SerialCMD[4].toInt(); 
                  setLEDColor(mLedNum, SerialCMD[3]);
                  if(tempTimer2 > 0){
                    setLEDTimer(tempTimer2 + 1);
                  }
                }
                tempTimer = 1;
              }// End Set 4 argu condition
            } else {                 // <----END of check register
              Serial.println("C:Not a valid register!");
            }
          } // end of set
          load_Config();                  /******************************/
        } // End of Set >=3 argu condition     /******************************/
      }   // End of if statement for cmd=set/get
      else {
        Serial.println("C:Not a valid command!");
      }
    }   //END of else statement for SerialArguments>1
  }     // End of ! if condition
}       // End of commandhandler



/*********************************** End New Serial Command Handler ***************************/


/******************************************LED Lights Handler******************************************/

void ledLightHandler(int lightSequence) {
  switch (lightSequence) {
    case 1:
      runningLights_Stat = false;
      fadeInOut_Stat = false;
      clearCallButtonLED();
      break;
    case 2:
      runningLights_Stat = false;
      fadeInOut_Stat = false;
      SetCallButtonLED(RED);
      break;
    case 3:
      runningLights_Stat = false;
      fadeInOut_Stat = false;
      SetCallButtonLED(GREEN);
      break;
    case 4:
      runningLights_Stat = false;
      fadeInOut_Stat = false;
      SetCallButtonLED(BLUE);
      break;
    case 5:
      runningLights_Stat = false;
      fadeInOut_Stat = false;
      SetCallButtonLED(YELLOW);
      break;
    case 6:
      runningLights_Stat = false;
      fadeInOut_Stat = false;
      SetCallButtonLED(ORANGE);
      break;
    case 7:
      runningLights_Stat = false;
      fadeInOut_Stat = false;
      SetCallButtonLED(CYAN);
      break;
    case 8:
      runningLights_Stat = false;
      fadeInOut_Stat = false;
      SetCallButtonLED(WHITE);
      break;
    case 9:
      runningLights_Stat = true;
      fadeInOut_Stat = false;
      runningColor = RED;
      break;
    case 10:
      runningLights_Stat = true;
      fadeInOut_Stat = false;
      runningColor = GREEN;
      break;
    case 11:
      runningLights_Stat = true;
      fadeInOut_Stat = false;
      runningColor = BLUE;
      break;
    case 12:
      runningLights_Stat = true;
      fadeInOut_Stat = false;
      runningColor = YELLOW;
      break;
    case 13:
      runningLights_Stat = true;
      fadeInOut_Stat = false;
      runningColor = ORANGE;
      break;
    case 14:
      runningLights_Stat = true;
      fadeInOut_Stat = false;
      runningColor = CYAN;
      break;
    case 15:
      runningLights_Stat = true;
      fadeInOut_Stat = false;
      runningColor = WHITE;
      break;
    case 16:
      runningLights_Stat = false;
      fadeInOut_Stat = true;
      ledBrightness = 0;
      fadeIN = true;
      fadeOUT = false;
      runningColor = RED;
      break;
    case 17:
      runningLights_Stat = false;
      fadeInOut_Stat = true;
      ledBrightness = 0;
      fadeIN = true;
      fadeOUT = false;
      runningColor = GREEN;
      break;
    case 18:
      runningLights_Stat = false;
      fadeInOut_Stat = true;
      ledBrightness = 0;
      fadeIN = true;
      fadeOUT = false;
      runningColor = BLUE;
      break;
    case 19:
      runningLights_Stat = false;
      fadeInOut_Stat = true;
      ledBrightness = 0;
      fadeIN = true;
      fadeOUT = false;
      runningColor = YELLOW;
      break;
    case 20:
      runningLights_Stat = false;
      fadeInOut_Stat = true;
      ledBrightness = 0;
      fadeIN = true;
      fadeOUT = false;
      runningColor = ORANGE;
      break;
    case 21:
      runningLights_Stat = false;
      fadeInOut_Stat = true;
      ledBrightness = 0;
      fadeIN = true;
      fadeOUT = false;
      runningColor = CYAN;
      break;
    case 22:
      runningLights_Stat = false;
      fadeInOut_Stat = true;
      ledBrightness = 0;
      fadeIN = true;
      fadeOUT = false;
      runningColor = WHITE;
      break;
  }
}


/************************************Key Equivalent Command Handler************************************/

KeyboardKeycode KeyEquivalentHandler(String cmdValue) {

  cmdValue.trim();

  if (cmdValue == "KEY_A")
  {
    return KEY_A;
  }
  if (cmdValue == "KEY_B")
  {
    return KEY_B;
  }
  if (cmdValue == "KEY_C")
  {
    return KEY_C;
  }
  if (cmdValue == "KEY_D")
  {
    return KEY_D;
  }
  if (cmdValue == "KEY_E")
  {
    return KEY_E;
  }
  if (cmdValue == "KEY_F")
  {
    return KEY_F;
  }
  if (cmdValue == "KEY_G")
  {
    return KEY_G;
  }
  if (cmdValue == "KEY_H")
  {
    return KEY_H;
  }
  if (cmdValue == "KEY_I")
  {
    return KEY_I;
  }
  if (cmdValue == "KEY_J")
  {
    return KEY_J;
  }
  if (cmdValue == "KEY_K")
  {
    return KEY_K;
  }
  if (cmdValue == "KEY_L")
  {
    return KEY_L;
  }
  if (cmdValue == "KEY_M")
  {
    return KEY_M;
  }
  if (cmdValue == "KEY_N")
  {
    return KEY_N;
  }
  if (cmdValue == "KEY_O")
  {
    return KEY_O;
  }
  if (cmdValue == "KEY_P")
  {
    return KEY_P;
  }
  if (cmdValue == "KEY_Q")
  {
    return KEY_Q;
  }
  if (cmdValue == "KEY_R")
  {
    return KEY_R;
  }
  if (cmdValue == "KEY_S")
  {
    return KEY_S;
  }
  if (cmdValue == "KEY_T")
  {
    return KEY_T;
  }
  if (cmdValue == "KEY_U")
  {
    return KEY_U;
  }
  if (cmdValue == "KEY_V")
  {
    return KEY_V;
  }
  if (cmdValue == "KEY_W")
  {
    return KEY_W;
  }
  if (cmdValue == "KEY_X")
  {
    return KEY_X;
  }
  if (cmdValue == "KEY_Y")
  {
    return KEY_Y;
  }
  if (cmdValue == "KEY_Z")
  {
    return KEY_Z;
  }
  if (cmdValue == "KEY_1")
  {
    return KEY_1;
  }
  if (cmdValue == "KEY_2")
  {
    return KEY_2;
  }
  if (cmdValue == "KEY_3")
  {
    return KEY_3;
  }
  if (cmdValue == "KEY_4")
  {
    return KEY_4;
  }
  if (cmdValue == "KEY_5")
  {
    return KEY_5;
  }
  if (cmdValue == "KEY_6")
  {
    return KEY_6;
  }
  if (cmdValue == "KEY_7")
  {
    return KEY_7;
  }
  if (cmdValue == "KEY_8")
  {
    return KEY_8;
  }
  if (cmdValue == "KEY_9")
  {
    return KEY_9;
  }
  if (cmdValue == "KEY_0")
  {
    return KEY_0;
  }
  if (cmdValue == "KEY_F1")
  {
    return KEY_F1;
  }
  if (cmdValue == "KEY_F2")
  {
    return KEY_F2;
  }
  if (cmdValue == "KEY_F3")
  {
    return KEY_F3;
  }
  if (cmdValue == "KEY_F4")
  {
    return KEY_F4;
  }
  if (cmdValue == "KEY_F5")
  {
    return KEY_F5;
  }
  if (cmdValue == "KEY_F6")
  {
    return KEY_F6;
  }
  if (cmdValue == "KEY_F7")
  {
    return KEY_F7;
  }
  if (cmdValue == "KEY_F8")
  {
    return KEY_F8;
  }
  if (cmdValue == "KEY_F9")
  {
    return KEY_F9;
  }
  if (cmdValue == "KEY_F10")
  {
    return KEY_F10;
  }
  if (cmdValue == "KEY_F11")
  {
    return KEY_F11;
  }
  if (cmdValue == "KEY_F12")
  {
    return KEY_F12;
  }
  if (cmdValue == "KEY_HOME")
  {
    return KEY_HOME;
  }
  if (cmdValue == "KEY_END")
  {
    return KEY_END;
  }
}

/************************************KeyboardKeycode Equivalent Converter************************************/

String keyboardKeycodetoString(KeyboardKeycode key) {

  if (key == KEY_A)
  {
    return "KEY_A";
  }
  if (key == KEY_B)
  {
    return "KEY_B";
  }
  if (key == KEY_C)
  {
    return "KEY_C";
  }
  if (key == KEY_D)
  {
    return "KEY_D";
  }
  if (key == KEY_E)
  {
    return "KEY_E";
  }
  if (key == KEY_F)
  {
    return "KEY_F";
  }
  if (key == KEY_G)
  {
    return "KEY_G";
  }
  if (key == KEY_H)
  {
    return "KEY_H";
  }
  if (key == KEY_I)
  {
    return "KEY_I";
  }
  if (key == KEY_J)
  {
    return "KEY_J";
  }
  if (key == KEY_K)
  {
    return "KEY_K";
  }
  if (key == KEY_L)
  {
    return "KEY_L";
  }
  if (key == KEY_M)
  {
    return "KEY_M";
  }
  if (key == KEY_N)
  {
    return "KEY_N";
  }
  if (key == KEY_O)
  {
    return "KEY_O";
  }
  if (key == KEY_P)
  {
    return "KEY_P";
  }
  if (key == KEY_Q)
  {
    return "KEY_Q";
  }
  if (key == KEY_R)
  {
    return "KEY_R";
  }
  if (key == KEY_S)
  {
    return "KEY_S";
  }
  if (key == KEY_T)
  {
    return "KEY_T";
  }
  if (key == KEY_U)
  {
    return "KEY_U";
  }
  if (key == KEY_V)
  {
    return "KEY_V";
  }
  if (key == KEY_W)
  {
    return "KEY_W";
  }
  if (key == KEY_X)
  {
    return "KEY_X";
  }
  if (key == KEY_Y)
  {
    return "KEY_Y";
  }
  if (key == KEY_Z)
  {
    return "KEY_Z";
  }
  if (key == KEY_1)
  {
    return "KEY_1";
  }
  if (key == KEY_2)
  {
    return "KEY_2";
  }
  if (key == KEY_3)
  {
    return "KEY_3";
  }
  if (key == KEY_4)
  {
    return "KEY_4";
  }
  if (key == KEY_5)
  {
    return "KEY_5";
  }
  if (key == KEY_6)
  {
    return "KEY_6";
  }
  if (key == KEY_7)
  {
    return "KEY_7";
  }
  if (key == KEY_8)
  {
    return "KEY_8";
  }
  if (key == KEY_9)
  {
    return "KEY_9";
  }
  if (key == KEY_0)
  {
    return "KEY_0";
  }
  if (key == KEY_F1)
  {
    return "KEY_F1";
  }
  if (key == KEY_F2)
  {
    return "KEY_F2";
  }
  if (key == KEY_F3)
  {
    return "KEY_F3";
  }
  if (key == KEY_F4)
  {
    return "KEY_F4";
  }
  if (key == KEY_F5)
  {
    return "KEY_F5";
  }
  if (key == KEY_F6)
  {
    return "KEY_F6";
  }
  if (key == KEY_F7)
  {
    return "KEY_F7";
  }
  if (key == KEY_F8)
  {
    return "KEY_F8";
  }
  if (key == KEY_F9)
  {
    return "KEY_F9";
  }
  if (key == KEY_F10)
  {
    return "KEY_F10";
  }
  if (key == KEY_F11)
  {
    return "KEY_F11";
  }
  if (key == KEY_F12)
  {
    return "KEY_F12";
  }
  if (key == KEY_HOME)
  {
    return "KEY_HOME";
  }
  if (key == KEY_END)
  {
    return "KEY_END";
  }
}

/*************************************************************************************/
void setLEDTimer(int timerValue) {
  LEDTimer = (timerValue - 1) * 1000;
  LEDTimerStart = millis();
  LEDTimerEN = true;
}

/*************************************************************************************/
void getspKey() {
  tempString = keyboardKeycodetoString(singlePressKey);
  if (singlePressCtrlPressed) {
    tempKeyString = tempKeyString + "ctrl+";
  }
  if (singlePressShiftPressed) {
    tempKeyString = tempKeyString + "shift+";
  }
  if (singlePressAltPressed) {
    tempKeyString = tempKeyString + "alt+";
  }
  Serial.println(tempKeyString + tempString);
  tempKeyString = "";
}

void getver() {
  Serial.println(sw + ":" + hw);
}

bool checkRegister(String regName) {
  for (int i = 0; i < 26; i++) {
    if (regName == regArray[i]) {
      return true;
    }
  }
  return false;
}

bool checkLEDParameter(int param) {
  if (0 < param && param < 23) {
    return true;
  }
  return false;
}

bool is_oneZero_ParamValid(String param) {
  if (param == "0" || param == "1") {
    return true;
  }
  return false;
}

/************LED App Control************/

void setLEDColor(int ledNum, String colorValue) {  //total 12 LEDs. ledNum = 0~11 ;  colorValue = GRB format, ex. 0xff0000 (GREEN)
  
  colorValue.toCharArray(colorValueArray, 8);
  hexValue = strtoul(colorValueArray, NULL, 16);
  ledLightHandler(1);
  runningLights_Stat = false;
  fadeInOut_Stat = false;
  strip.setPixelColor(ledNum, hexValue);
  strip.show();
}

void setFixLED(String colorValue) {
  colorValue.toCharArray(colorValueArray, 8);
  hexValue = strtoul(colorValueArray, NULL, 16);
  SetCallButtonLED(hexValue);
}

void setRunningLED(String colorValue) {
  colorValue.toCharArray(colorValueArray, 8);
  hexValue = strtoul(colorValueArray, NULL, 16);
  runningLights_Stat = true;
  fadeInOut_Stat = false;
  runningColor = hexValue;
}

void setFadeColor(String colorValue) {
  colorValue.toCharArray(colorValueArray, 8);
  hexValue = strtoul(colorValueArray, NULL, 16);
  runningLights_Stat = false;
  fadeInOut_Stat = true;
  ledBrightness = 0;
  fadeIN = true;
  fadeOUT = false;
  runningColor = hexValue;
}
/***********************Unique ID*************************/
void getUniqueID() {
  Serial.print("UniqueID: ");
  for (size_t i = 0; i < UniqueIDsize; i++)
  {
    if (UniqueID[i] < 0x10)
      Serial.print("0");
    Serial.print(UniqueID[i], HEX);
    Serial.print(" ");
  }
  Serial.println();
}